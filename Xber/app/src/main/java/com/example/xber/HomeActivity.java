package com.example.xber;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.google.android.material.navigation.NavigationView;

public class HomeActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, NavigationView.OnNavigationItemSelectedListener {
    Button continue_button;
    Spinner day_dropdown;


    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    Toolbar toolbar;
    NavigationView navigationV;

    @Override
        protected void onCreate (Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_home);
            LayoutInflater inflater = getLayoutInflater();
            DrawerLayout drawerLayout = (DrawerLayout) inflater.inflate(R.layout.activity_main2,null);
           ConstraintLayout constraintLayout = (ConstraintLayout) inflater.inflate(R.layout.activity_home,null);

            continue_button = findViewById(R.id.continue_button);
            final Spinner city_dropdown = findViewById(R.id.spinner1);
//create a list of items for the spinner.
            String[] cities = new String[]{"select city", "mugla", "eskisehir", "aydin"};
//create an adapter to describe how the items are displayed, adapters are used in several places in android.
//There are multiple variations of this, but this is the basic variant.
            final ArrayAdapter<String> cityAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, cities);
//set the spinners adapter to the previously created one.
            city_dropdown.setAdapter(cityAdapter);

            day_dropdown = findViewById(R.id.spinner_day);
//create a list of items for the spinner.
            String[] days = new String[]{"select day count", "1", "2", "3", "4", "5", "6", "7"};
//create an adapter to describe how the items are displayed, adapters are used in several places in android.
//There are multiple variations of this, but this is the basic variant.
            ArrayAdapter<String> day_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, days);
//set the spinners adapter to the previously created one.
            day_dropdown.setAdapter(day_adapter);
            // city_dropdown.setOnItemClickListener((AdapterView.OnItemClickListener) this);
            continue_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(HomeActivity.this, CarSelectActivity.class);
                    if (!city_dropdown.getSelectedItem().equals("select city") && !day_dropdown.getSelectedItem().equals("select day count")) {
                        intent.putExtra("city", (String) city_dropdown.getSelectedItem());
                        intent.putExtra("day", (String) day_dropdown.getSelectedItem());
                        startActivity(intent);
                    } else {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(HomeActivity.this);

                        // alert dialog başlığını tanımlıyoruz.
                        alertDialogBuilder.setTitle("Wrong selection");

                        // alert dialog özelliklerini oluşturuyoruz.
                        alertDialogBuilder
                                .setMessage("You have to choose a city and day number...")
                                .setCancelable(false)
                                .setIcon(R.mipmap.wrong1)
                                // Evet butonuna tıklanınca yapılacak işlemleri buraya yazıyoruz.
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        // alert dialog nesnesini oluşturuyoruz
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // alerti gösteriyoruz
                        alertDialog.show();
                    }
                }
            });
        }

        @Override
        public void onItemSelected (AdapterView < ? > parent, View view,int position, long id){
            parent.getItemAtPosition(position);
        }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }
}
