package com.example.xber;

import android.graphics.Bitmap;

import java.util.ArrayList;

public interface XberListener {

    public void sendDataBase(Bitmap gridView, int cost_of_car, String car_name);
}
