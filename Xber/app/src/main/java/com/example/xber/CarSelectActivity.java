package com.example.xber;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

public class CarSelectActivity extends Activity implements XberView,arrayAlici {
    ProgressDialog PD;
    Presenter presenter;
    String[] dataBase;
    ArrayList<String > URL_IMG = new ArrayList<>();
    GridView gridView;
    CustomAdapter customAdapter;
    ArrayList<Bitmap> Images = new ArrayList<>();
    ArrayList<Integer> costs = new ArrayList<Integer>();
    ArrayList<ArrayList<String> > attributes = new ArrayList<>();
    String[] Database;
    ArrayList<Integer> car_ids = new ArrayList<>();
    private static final int  REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE ={
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int PERMISSION_REQUEST_CODE = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //textView.setText(getIntent().getStringExtra("city") + "" + getIntent().getStringExtra("day"));
        int  permission = ActivityCompat.checkSelfPermission(
                CarSelectActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE

        );

        if(permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(CarSelectActivity.this,PERMISSIONS_STORAGE,REQUEST_EXTERNAL_STORAGE);
        }

        //gridView = findViewById(R.id.myview);
        presenter = new Presenter(this,this);
        try {
            getDatabtn(""+getIntent().getStringExtra("city"));
        }catch (Exception e){

        }




    }

    public void getImages(){
        gridView = new GridView(this);
        gridView.setNumColumns(3);
        setContentView(gridView);

        downloadTask downloadTask1  = new downloadTask(this,this,gridView);
        String[] imageURL = new String[URL_IMG.size()];
        for( int t = 0; t < imageURL.length;t++){
            imageURL[t] = URL_IMG.get(t);
        }
        downloadTask1.execute(imageURL);

    }

    public void getDatabtn(String city){

        background bg = new background(this,this);
        System.out.println(bg.execute(city));

    }


    @Override
    public GridView getLayout() {
        return gridView;
    }


    @Override
    public void setDatabase(Bitmap image, int cost_of_car,String car_name) {
    }

    public void addToAttributesArray(String... attributes1 ){
        ArrayList<String> a1 = new ArrayList<>();
        for(int q = 0; q < attributes1.length;q++) {

            a1.add(attributes1[q]);
            System.out.println("attributessss: "+ attributes1[q]);
        }
        attributes.add(a1);


    }

    @Override
    public void arrayaekle(String s) {
        if(s != null) {
            Database = s.split("\\s");
            System.out.println("databaseden çekilen veriler:" + s);
            System.out.println("database uzunluğu :"+ Database.length);
            int a = 1;
            int b = 2;
            int id = 0;
            int cost_of_car = 3;
            int car_name = 4;
            int gear_feauture = 5;
            int engine_type = 6;
            int seat_number = 7;
            int fuel_rate = 8;
            while (a < Database.length) {
                int x = Integer.parseInt(getIntent().getStringExtra("day"));
                costs.add(Integer.parseInt(Database[a]) *x);
                URL_IMG.add(Database[b]);
                addToAttributesArray(Database[id],Database[car_name],Database[gear_feauture],Database[engine_type],
                        Database[seat_number],Database[fuel_rate],Database[cost_of_car],Database[a]);
             //   System.out.println(costs.get(0));
                car_ids.add(Integer.parseInt(Database[id]));
                a += 9;
                b += 9;
                cost_of_car+=9;
                id+=9;
                car_name+=9;
                gear_feauture+=9;
                engine_type+=9;
                seat_number+=9;
                fuel_rate+=9;
            }
            getImages();//it will put 0th element in IMAGES array
        }else{
            System.out.println("buraya geldi");
            LinearLayout layout = new LinearLayout(this);
            layout.setOrientation(LinearLayout.VERTICAL);

            LinearLayout.LayoutParams params =new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);


            Button tekrarDeneyinbtn = new Button(this);
            params.gravity = Gravity.CENTER;
            tekrarDeneyinbtn.setLayoutParams(params);

            tekrarDeneyinbtn.setText("Try Again");
            tekrarDeneyinbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDatabtn(getIntent().getStringExtra("city"));
                }
            });


            TextView internetConnection = new TextView(this);
            internetConnection.setText("make sure you have internet connection");
            internetConnection.setLayoutParams(params);

            layout.addView(internetConnection);
            layout.addView(tekrarDeneyinbtn);
            setContentView(layout);

        }if(Database == null && s ==null){
            LinearLayout layout = new LinearLayout(this);
            layout.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams params =new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER;
            TextView noCar = new TextView(this);
            noCar.setText("There is no car to show");
            noCar.setLayoutParams(params);
            layout.addView(noCar);
            setContentView(layout);
        }

    }

    @Override
    public void putImage(ArrayList<Bitmap> b) {

            Images = b;
            System.out.println(Images.size());
        customAdapter = new CustomAdapter(CarSelectActivity.this,Images,costs,this,car_ids);

        runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    gridView.setAdapter(customAdapter);

                }
            });


    }

    public void loadCarAttributes(final GridView gridView, int i,final Bitmap image){
        LayoutInflater layoutInflater = getLayoutInflater();
        ConstraintLayout constraintLayout = (ConstraintLayout) layoutInflater.inflate(R.layout.car_attributes,null);
        int index=0;
        ArrayList<String > lists = new ArrayList<>();
        for( int h = 0; h < car_ids.size();h++){
            if(i == Integer.parseInt(attributes.get(h).get(0))){
                index = h;
                break;
            }

        }
        String  x = getIntent().getStringExtra("day");
        final int cost_of_car = Integer.parseInt(attributes.get(index).get(7))*Integer.parseInt(x);
        Toast.makeText(getApplicationContext(),attributes.get(index).get(7),Toast.LENGTH_LONG);
        System.out.println("cost_of_car" + cost_of_car);
        lists.add("Geaebox: " +attributes.get(index).get(2).split("_")[0] +", " +attributes.get(index).get(2).split("_")[1] + " " +attributes.get(index).get(2).split("_")[2]);//2,3,4,5
        lists.add("Engine type: "+attributes.get(index).get(3));//2,3,4,5
        lists.add("Seat number: "+attributes.get(index).get(4));//2,3,4,5
        lists.add("Fuel consumption: "+attributes.get(index).get(5));//2,3,4,5

        TextView textView = constraintLayout.findViewById(R.id.car_name);
        textView.setText(attributes.get(index).get(1).split("_")[0] + " "+ attributes.get(index).get(1).split("_")[1]);
        final String car_name  = (String) textView.getText();
        LinearLayout linearLayout = constraintLayout.findViewById(R.id.attributes_of_car);
        for(int l = 0; l <lists.size();l++){
            TextView textView1 = new TextView(this);

            textView1.setText(lists.get(l));
            linearLayout.addView(textView1);
        }

        Button backButton = constraintLayout.findViewById(R.id.back);


        setContentView(constraintLayout);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentView(gridView);
            }
        });
        Button continue_button = constraintLayout.findViewById(R.id.continue_button);

        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ödeme_ekranı
                odeme_ekrani(cost_of_car,image,car_name);
                Toast.makeText(getApplicationContext()," qweqw",Toast.LENGTH_LONG);
            }
        });

    }
    void odeme_ekrani(final int cost_of_car, final Bitmap image, final String car_name){
        LayoutInflater layoutInflater = getLayoutInflater();
        final ConstraintLayout constraintLayout = (ConstraintLayout) layoutInflater.inflate(R.layout.odeme_ekrani,null);
        TextView textView = constraintLayout.findViewById(R.id.fiyat_);
        textView.setText("Your debt is : $" + cost_of_car);

        setContentView(constraintLayout);
      final   EditText enterText = constraintLayout.findViewById(R.id.card_number_plain);
        Button continue_button = constraintLayout.findViewById(R.id.continue_button);
        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isStoragePermissionGranted()){
                    presenter.sendDataBase(image,cost_of_car,car_name);
                }
                Intent intent = new Intent(CarSelectActivity.this,MapsActivity.class);
                startActivity(intent);
            }
        });

        Button cash_payment = constraintLayout.findViewById(R.id.cash_payment);
        cash_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent i = new Intent(CarSelectActivity.this,History.class);
                //startActivity(i);
                if(isStoragePermissionGranted()){
                    presenter.sendDataBase(image,cost_of_car,car_name);
                }
                Intent intent = new Intent( CarSelectActivity.this,MapsActivity.class);
                startActivity(intent);
            }
        });
    }



    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("qw","Permission is granted");
                Toast.makeText(getApplicationContext(),"kabul edildi", Toast.LENGTH_LONG);
                System.out.println("kabul edildi");
                return true;
            } else {

                Log.v("e","Permission is revoked");
                Toast.makeText(getApplicationContext(),"red edildi", Toast.LENGTH_LONG);
                System.out.println("red edildi");
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("qwe","Permission is granted");
            Toast.makeText(getApplicationContext(),"mantar edildi", Toast.LENGTH_LONG);
            System.out.println("mantar edildi");
            return true;
        }
    }








    public void setOnClickme(ImageButton imgButton, final int i, final Bitmap image){
        imgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            loadCarAttributes(gridView,i,image);


            }
        });



    }


    class downloadTask extends AsyncTask<String,Integer, ArrayList> {

        arrayAlici alici;
        Context context;
        GridView gridView;

        public downloadTask(arrayAlici alici,Context context,GridView gridView){
            this.context = context;
            this.gridView =gridView;
            this.alici = alici;
        }
        @SuppressLint("WrongThread")
        @Override
        protected ArrayList<Bitmap> doInBackground(String... strings) {
            ArrayList<Bitmap> bitmapArrayList = new ArrayList<>();
            String fileName = "temp.jpg";
            String  imagePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + fileName;

            int b = 0;
            while(b < strings.length) {
                bitmapArrayList.add(download(strings[b], imagePath));/**/
                b++;
            }



            return bitmapArrayList;
        }

        @Override
        protected void onPostExecute(ArrayList arrayList) {
            alici.putImage(arrayList);
            PD.dismiss();
        }



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            PD = new ProgressDialog(CarSelectActivity.this);
            PD.setIndeterminate(false);
            PD.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            PD.setCancelable(false);
            PD.setTitle("Pictures downloading");
            PD.setMessage("please wait..");
            PD.show();
        }

        /////////////////////////
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            PD.setProgress(values[0]);//before download
        }


        //////////////////////////////////////////////////////

        private Bitmap download(String strurl, String imagePath) {
            try {
                URL url1 = new URL(strurl);

                URLConnection connection = url1.openConnection();
                connection.connect();
                int filesize = connection.getContentLength();
                InputStream inputStream  = new BufferedInputStream(url1.openStream(),8131);//to read data from file or location
                OutputStream outputStream = new FileOutputStream(imagePath);

                int total = 0;
                byte   data[] = new byte[1024]; //we will get the content of this array
                int size;
                while((size =inputStream.read(data)) != -1)//if -1, we are end of thee file
                {
                    outputStream.write(data , 0,size);
                    total += size;
//////////
                    int percentage= (int) (((double)  total/filesize) * 100);
                    publishProgress(percentage);
///////////////
                }
                outputStream.close();
                inputStream.close();
            } catch (IOException e) {
                Log.e("Download",strurl,e);
                e.printStackTrace();
            }

            return BitmapFactory.decodeFile(imagePath);



        }
    }
}










class background extends AsyncTask<String , Void,String>{
    Context context;
    arrayAlici alici;
    public background(arrayAlici alici, Context context){
        this.context = context;
        this.alici = alici;
    }
    @Override
    protected void onPreExecute() {

    }

    @Override
    protected void  onPostExecute(String s) {

        alici.arrayaekle(s);
    }

    @Override
    protected String doInBackground(String... voids) {
        String result ="";
        String Sehir = voids[0];


        String connection = "http://ogiguiproject.dx.am/index.php";
        try {
            URL url = new  URL(connection);
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoInput(true);
                httpURLConnection.setDoOutput(true);

                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String data = URLEncoder.encode("sehir","UTF-8")+"="+URLEncoder.encode(Sehir,"UTF-8");
                writer.write(data);
                writer.flush();
                writer.close();
                outputStream.close();

                InputStream ips = httpURLConnection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(ips,"ISO-8859-1"));
                String line ="";
                while ((line = reader.readLine()) != null){
                    result += line;

                }

                reader.close();
                ips.close();
                httpURLConnection.disconnect();
                return result;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }
}

interface arrayAlici{
    public void arrayaekle(String  s);
    public void putImage(ArrayList<Bitmap> b);
}
