package com.example.xber;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.nfc.Tag;
import android.os.Build;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.GridView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;

public class Presenter implements XberListener, View.OnClickListener {
    XberView xberView;
    Context context;
    Model history;
    public Presenter(XberView xberView,Context context){
        this.xberView = xberView;
        this.context = context;
        history = new Model(this,context);

    }

    @Override
    public void sendDataBase(Bitmap image, int cost_of_car, String car_name) {
            String cost_of_car1 = String.valueOf(cost_of_car);
            this.history.SaveImage(image,cost_of_car1,car_name);

    }
    public ArrayList<String > getDatas(){
        return  history.read_file();
    }
    @Override
    public void onClick(View v) {
        Toast.makeText(context,"selam",Toast.LENGTH_LONG).show();
    }

}
