package com.example.xber;


import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

public class MainInt extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


     DrawerLayout drawerLayout;
     ActionBarDrawerToggle drawerToggle;
     Toolbar toolbar;
    NavigationView navigationV;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawer);
        drawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.open_drawer,R.string.close_drawer); // settings
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.setDrawerIndicatorEnabled(true); // important
        drawerToggle.syncState();


        navigationV = findViewById(R.id.navigationView);
        navigationV.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.logout:
                Toast.makeText(this,"Home",Toast.LENGTH_SHORT).show();
                Intent intent1  = new Intent(MainInt.this,MainActivity.class);
                startActivity(intent1);
                break;
            case R.id.history:
                Intent intent2 = new Intent(MainInt.this, History.class);
                startActivity(intent2);
                break;
            case R.id.home:
                Intent intent3 = new Intent(MainInt.this, HomeActivity.class);
                startActivity(intent3);
                break;
        }
        return false;
    }


}