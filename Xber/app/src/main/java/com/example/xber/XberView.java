package com.example.xber;

import android.graphics.Bitmap;
import android.text.Layout;
import android.widget.GridView;

public interface XberView {
    public GridView getLayout();
    public void setDatabase(Bitmap gridView, int cost_of_car, String car_name);
}
