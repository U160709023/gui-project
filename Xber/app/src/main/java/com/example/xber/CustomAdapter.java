package com.example.xber;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter {
    Activity appContext;
    LayoutInflater layoutInflater;
    ArrayList<Bitmap> list;
    ArrayList<Integer > Costs;
    CarSelectActivity carAttributes;
    ArrayList<Integer> car_ids;
    public CustomAdapter(Activity context, ArrayList<Bitmap> list, ArrayList<Integer> costs, CarSelectActivity carAttributes,ArrayList<Integer> car_ids) {
        this.appContext = context;
        this.car_ids = car_ids;
        this.list = list;
        this.Costs = costs;
        this.carAttributes = carAttributes;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);//Nereye yüklemek istediğimizi belirttik
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View myView;
        myView = layoutInflater.inflate(R.layout.grids_view, null); //neyi yüklemek istiyoruz
        ImageButton icon = (ImageButton) myView.findViewById(R.id.myicon);

        icon.setImageBitmap(list.get(i));
        TextView txtView = myView.findViewById(R.id.textView);
       // txtView.setGravity(View.TEXT_ALIGNMENT_CENTER);
        txtView.setText("$"+Costs.get(i));
        icon.setId(car_ids.get(i));
        this.setOnClickme(icon,icon.getId(),list.get(i));

        return myView;
    }

    public void setOnClickme(final ImageButton imgButton, final int i,final Bitmap image){
        imgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // Toast.makeText(appContext,"benim adım " + i ,Toast.LENGTH_SHORT).show();
                carAttributes.setOnClickme(imgButton,i,image);
            }
        });

    }



}
