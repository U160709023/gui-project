package com.example.xber;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.widget.GridView;
import android.widget.ListView;

import java.util.ArrayList;


public class History extends AppCompatActivity  implements XberView{
    ArrayList<Bitmap> images = new ArrayList<>();
    ArrayList<String >car_names = new ArrayList<>();
    ArrayList<Integer> car_cost = new ArrayList<>();
    ArrayList<String > imgnames = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history);
        Presenter presenter = new Presenter(this,this);
        ArrayList<String > datas = new ArrayList<>(presenter.getDatas());//imgname, carname,car cost
        int a = 1;
        int b = 2;
        System.out.println("arabanın fiyatı" + datas.get(b));
        int c = 0;
        while(b < datas.size()){
            int fiyat = Integer.parseInt(datas.get(b));
            car_names.add(datas.get(a));
            car_cost.add(fiyat);
            imgnames.add(datas.get(c));
            images.add(read_images(datas.get(c)));
            c+=3;
            a+=3;
            b+=3;
        }

        ListView listView = findViewById(R.id.listView1);
        CustomAdapterForHistory customAdapterForHistory = new CustomAdapterForHistory(this,images,car_cost,car_names);
        listView.setAdapter(customAdapterForHistory);
    }
    public Bitmap read_images(String imgnames){
        String photoPath = Environment.getExternalStorageDirectory() + "/saved_images/" + imgnames;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(photoPath, options);
        return bitmap;
    }
    @Override
    public GridView getLayout() {
        return null;
    }

    @Override
    public void setDatabase(Bitmap gridView, int cost_of_car, String car_name) {

    }
}
