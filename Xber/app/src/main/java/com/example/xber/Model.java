package com.example.xber;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;

public class Model {
    private static final int PERMISSION_REQUEST_CODE = 100;

    XberListener xberListener;
    Context context;
    public Model(XberListener xberListener, Context context){
        this.xberListener = xberListener;
this.context = context;
    }


    public void SaveImage(Bitmap finalBitmap,
                          String cost_of_car, String car_name) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images/");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-"+ n +".jpg";
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


        write_data(fname,car_name,cost_of_car);
    }

    public void write_data(String image_adi,String car_name, String car_cost){
            car_name = car_name.split("\\s")[0] + car_name.split("\\s")[1];
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkPermission()) {
                        File sdcard = Environment.getExternalStorageDirectory();
                        File dir = new File(sdcard.getAbsolutePath() + "/text/");
                        dir.mkdir();
                        File file = new File(dir, "sample4.txt");
                        FileOutputStream os = null;
                        try {
                            FileWriter fw = new FileWriter(file,true);

                            //os = new FileOutputStream(file,true);
                            PrintWriter pr = new PrintWriter(new BufferedWriter(fw));
                            pr.println(image_adi + " " +car_name + " " + car_cost);
                            pr.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        requestPermission(); // Code for permission
                    }
                } else {
                    File sdcard = Environment.getExternalStorageDirectory();
                    File dir = new File(sdcard.getAbsolutePath() + "/text/");
                    if(!dir.exists()){
                        dir.mkdir();}
                    File file = new File(dir, "sample4.txt");
                    System.out.println("directory :" + dir);
                    FileOutputStream os = null;
                    try {
                        os = new FileOutputStream(file);
                        FileWriter dr = new FileWriter(file,true);
                        dr.write(car_name + " "+ car_cost + "\n");

                        OutputStreamWriter outDataWriter = new OutputStreamWriter(os);
                        //outDataWriter.append(enterText.getText().toString().getBytes() + "\n");
                        outDataWriter.close();
                        dr.close();
                        os.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

    }


    public ArrayList<String > read_file(){
        ArrayList<String > datas = new ArrayList<>();
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (checkPermission()) {
                    File sdcard = Environment.getExternalStorageDirectory();
                    File dir = new File(sdcard.getAbsolutePath() + "/text/");
                    if(dir.exists()) {
                        File file = new File(dir, "sample4.txt");
                        FileOutputStream os = null;
                        StringBuilder text = new StringBuilder();
                        try {
                            BufferedReader br = new BufferedReader(new FileReader(file));
                            String line;

                            while ((line = br.readLine()) != null) {
                                text.append(line);
                                datas.add(line.split("\\s")[0]);
                                datas.add(line.split("\\s")[1]);
                                datas.add(line.split("\\s")[2]);


                                System.out.println("dosyafaki yazı" + line);

                            }
                            br.close();
                        } catch (IOException e) {
                            //You'll need to add proper error handling here
                        }
                    }
                } else {
                    requestPermission(); // Code for permission
                }
            } else {
                File sdcard = Environment.getExternalStorageDirectory();
                File dir = new File(sdcard.getAbsolutePath() + "/text/");
                if(dir.exists()) {
                    File file = new File(dir, "sample4.txt");
                    FileOutputStream os = null;
                    StringBuilder text = new StringBuilder();
                    try {
                        BufferedReader br = new BufferedReader(new FileReader(file));
                        String line;
                        while ((line = br.readLine()) != null) {
                            text.append(line);
                            text.append('\n');
                        }
                        br.close();
                    } catch (IOException e) {
                        //You'll need to add proper error handling here
                    }
                }
            }
        }
return datas;
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(context, "Write External Storage permission allows us to create files. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }






}
