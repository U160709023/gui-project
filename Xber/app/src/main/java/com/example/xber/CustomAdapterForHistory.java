package com.example.xber;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.Layout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;

public class CustomAdapterForHistory extends BaseAdapter {
    Activity appContext;
    LayoutInflater layoutInflater;
    ArrayList<Bitmap> list;
    ArrayList<Integer > Costs;
    ArrayList<String > car_names;
    public CustomAdapterForHistory(Activity context, ArrayList<Bitmap> list, ArrayList<Integer> costs,ArrayList<String > car_names) {
        this.appContext = context;
        this.car_names = car_names;
        this.list = list;
        this.Costs = costs;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);//Nereye yüklemek istediğimizi belirttik
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View myView;
        myView = layoutInflater.inflate(R.layout.particle_of_history, null); //neyi yüklemek istiyoruz
        ImageView icon = myView.findViewById(R.id.resim_koy);
        icon.setImageBitmap(list.get(i));

        TextView txtView = myView.findViewById(R.id.adi_ve_fiyati);
        // txtView.setGravity(View.TEXT_ALIGNMENT_CENTER);
        txtView.setText(car_names.get(i) + " =>" +"$"+Costs.get(i));
        txtView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        txtView.setGravity(Gravity.CENTER);




        return myView;
    }


    }




